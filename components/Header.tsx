import Logo from "@/icons/logo.svg";
import Image from "next/image";
import { ThemeToggler } from "./ThemeToggler";
import { Button } from "./ui/button";
import { SignInButton, UserButton, auth } from "@clerk/nextjs";
const Header = () => {
    const { userId }: { userId: string | null } = auth();
    return (
        <header className="w-full">
            <div className="w-full flex justify-between items-center p-4 border-b border-gray-100 dark:border-gray-700 hover:shadow-lg dark:hover:shadow-slate-700">
                <div className="flex flex-row justify-start items-center">
                    <div className='relative w-12 h-12'>
                        <Image src={Logo} alt="trello logo" fill />
                    </div>
                    <h1 className="text-5xl font-bold ml-6 hidden lg:block text-black dark:text-white">Trello</h1>
                </div>
                <div className="flex flex-row justify-end items-center space-x-4">
                    {userId ?
                        <UserButton
                            afterSignOutUrl="/"
                        />
                        :
                        <Button variant={"outline"}>
                            <SignInButton
                                mode="modal"
                            />
                        </Button>
                    }
                    <ThemeToggler />
                </div>
            </div>
        </header>
    )
}

export default Header